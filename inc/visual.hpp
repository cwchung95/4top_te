#include <sys/ioctl.h>
#include <stdio.h>
#include <unistd.h>

struct winsize w;

void ProgressBar(float progress){
	ioctl(0,TIOCGWINSZ,&w);
	int cols = w.ws_col;
	int barWidth = cols-26;
	int pos = progress*barWidth;
	TString space = "";
	for(int sp = 0 ; sp < 10 - strlen(Form("%.2f",progress*100.00)) ; sp++) space = space + " ";
	cout << " processing " << space << Form("%.2f",progress*100.00) << "% [";
	for(int i=0; i<barWidth; i++){
		if(i <= pos) cout << "■";
		else cout << " ";
	}
	cout << "]\r";
	cout.flush();
}
