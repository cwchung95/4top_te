#include <iostream>
#include <vector>
#include "TString.h"

using namespace std;

vector<TString> periods(TString year){
	vector<TString> ret;
	if(year=="2016"){
		ret.push_back("C");
		ret.push_back("D");
		ret.push_back("E");
		ret.push_back("F");
		ret.push_back("G");
		ret.push_back("H");
	}
	else if(year=="2017"){
		ret.push_back("B");
		ret.push_back("C");
		ret.push_back("D");
		ret.push_back("E");
		ret.push_back("F");
	}
	else if(year=="2018"){
		ret.push_back("A");
		ret.push_back("B_bef317509");
		ret.push_back("B_aft317509");
		ret.push_back("C");
		ret.push_back("D");
	}
	return ret;
}
