#!/bin/sh

for year in  2016 2017 2018
	do echo ${year}
	./run/trig_eff_2D.exe $1 ${year} v6
	./run/trig_eff_1D.exe $1 on ${year} v6 
	./run/trig_eff_1D.exe $1 off ${year} v6 
done
