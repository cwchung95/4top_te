#include <iostream>
#include <fstream>
#include <vector>
#include <string>
#include <sstream>
#include <stdlib.h>

#include "TChain.h"
#include "TROOT.h"
#include "TFile.h"
//#include "TString.h"

#include "inc/periods.hpp"

using namespace std;
void skim_process(TString file, TString output);

void skim(TString year = "2018", TString vnanoAOD = "v6"){
	cout << " year            : " << year << endl;
	cout << " nanoAOD version : " << vnanoAOD << endl;
	vector<TString> arr_period = periods(year);
	
	for(int index=0; index<arr_period.size();index++){
		skim_process("/master-data/common_data/"+year+vnanoAOD+"/"+arr_period.at(index)+"/*.root", year+vnanoAOD+"_"+arr_period.at(index)+"_Data.root");
		cout<<year+vnanoAOD+"_"+arr_period.at(index)+"_Data.root"<<" created"<<endl;
	}
}

void skim_process(TString file, TString output){
	TChain ch("Events");
	ch.Add(file);
	
	TString outfile = file;
	TFile *f = new TFile("skimmed_files/"+output,"recreate");
	
	TString cuts = "(HLT_IsoMu27 || HLT_IsoMu24) && Sum$(Jets_pt>35&&abs(Jets_eta)<2.4)>=7";
	TTree *ctree = ch.CopyTree(cuts);
	f->cd();
	if(ctree) ctree->Write();
	f->Close();
}

