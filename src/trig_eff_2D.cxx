#include <iostream>
#include <stdlib.h>
#include <vector>

#include "TChain.h"
#include "TROOT.h"
#include "TFile.h"
#include "TH2D.h"
#include "TCanvas.h"
#include "TStyle.h"

#include "periods.hpp"
#include "objects.hpp"
#include "visual.hpp"

using namespace std;

int main(int argc, char *argv[]){
  if(strcmp(argv[1],"--help")==0){
    cout << " ./run/trig_eff_2D.exe [pureHT trigger on/off] [year] [nanoAOD version] " << endl;
  }
  else if(argc<4){
    cout << " [Error] Invalid arguments numbers! " << endl;
    cout << " --help may give you some help :)" << endl;
  }
  else{
	cout << " pure HT trigger : " << argv[1] << endl; 
  	cout << " year            : " << argv[2] << endl;
	cout << " nanoAOD version : " << argv[3] << endl;
	
	TString PFHT(argv[1]);
	TString year(argv[2]);
	TString vnanoAOD(argv[3]);

	bool pureHT;

	if(PFHT=="on"){
		pureHT=true;
	}
	else{
		pureHT=false;
	}

	vector<TString> arr_per;
	//vector<TChain> arr_ch; 
	
	arr_per = periods(year);

	
	
	TH2D *h2_den = new TH2D("h2_den","h2_den",3,1.5,4.5,6,6.5,12.5);
	TH2D *h2_num = new TH2D("h2_num","h2_num",3,1.5,4.5,6,6.5,12.5);

	int tot_ent(0), tot_ient(0);	
	for( auto per : arr_per ){
		TChain *chs = new TChain("Events");// = new TChain("Events");
		chs->Add("skimmed_files/"+year+vnanoAOD+"_"+per+"*_Data.root");
		tot_ent += chs->GetEntries();
	}


	for( auto per : arr_per ){
		TChain *ch = new TChain("Events");// = new TChain("Events");
		ch->Add("skimmed_files/"+year+vnanoAOD+"_"+per+"_Data.root");
		vector<void*> Sys_var = Syst_var(ch, year, per);

		for( int ientry=0 ; ientry < ch->GetEntries() ; ientry++){
			int nj(0), nb(0);
			float ht(0);
			bool cut = true;
			bool trig= true;

			GetSyst(ch, year, per, ientry, &nj, &nb, &ht, &cut, &trig, true, pureHT, Sys_var);

			nb = min(nb,4);
			nj = min(nj,12);	
			h2_den->Fill(nb, nj, cut);
			h2_num->Fill(nb, nj, cut && trig);
			float progress = float(tot_ient)/(tot_ent);
			ProgressBar(progress);
			tot_ient++;
		}
	}
	cout<<"\n Finished" <<endl;
	TH2D *h2_eff = dynamic_cast<TH2D*>(h2_num->Clone("h2_eff"));
	h2_eff->Divide(h2_num,h2_den,1,1,"B");

	TFile *f = new TFile("hist_root/"+year+vnanoAOD+".root","recreate");

	TCanvas *c = new TCanvas("c","c",1500,600);
	c->Divide(3,1);
	c->cd(1);
	gStyle->SetPaintTextFormat("0.2f");
	h2_eff->SetTitle("h2_eff");
	h2_eff->SetStats(0);
	h2_eff->SetMarkerSize(2);
	h2_eff->Draw("colz text e");
	h2_eff->Write();
	c->cd(2);
	h2_den->SetStats(0);
	h2_den->Draw("same colz text e");
	h2_den->Write();
	c->cd(3);
	h2_num->SetStats(0);
	h2_num->Draw("same colz text e");
	c->Print("Plots/"+year+vnanoAOD+".pdf");
	
	f->Close();
	cout<<" Root File Maded in hist_root/"<<endl;
  }
}
