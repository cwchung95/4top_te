#include <iostream>
#include <fstream>
#include <vector>
#include <string>
#include <sstream>
#include <stdlib.h>

#include "TChain.h"
#include "TMath.h"
#include "TROOT.h"
#include "TFile.h"
#include "TCanvas.h"
#include "TH2.h"

#include "periods.hpp"
#include "objects.hpp"
#include "visual.hpp"

int main(int argc, char *argv[]){
	cout << " year            : " << argv[1] << endl;
	cout << " nanoAOD version : " << argv[2] << endl;

	TString year  = argv[1];
	TString vnano = argv[2];
	
	vector<TString> vec_per = periods(year);
	
	int tot_ent(0);

	for(auto period : vec_per){
		TChain *chs = new TChain("Events");
		chs->Add("skimmed_files/"+year+vnano+"_"+period+"_Data.root");
		tot_ent = tot_ent + chs->GetEntries();		
	}

	TH2D *h2_pureHT = new TH2D("h2_pureHT","h2_pureHT",3,1.5,4.5,6,6.5,12.5);
	TH2D *h2_NoHT   = new TH2D("h2_NoHT","h2_NoHT",3,1.5,4.5,6,6.5,12.5); 
	int tot_ient(0);

	for(auto period : vec_per){
		TChain *ch = new TChain("Events");
		ch->Add("skimmed_files/"+year+vnano+"_"+period+"_Data.root");
		vector<void *> Sys_var = Syst_var(ch, year, period);
		for(int ientry = 0 ; ientry < ch->GetEntries() ; ientry++){
			int nb(0), nj(0);
			float ht(0), cor(0);
			bool cut(true), trig(true);
			GetYield(ch, year, period, ientry, &nj, &nb, &ht, &cut, &trig, true, true, Sys_var);
			nb = min(nb,4);
			nj = min(nj,12);
			h2_pureHT->Fill(nb,nj,cut && trig);

			GetYield(ch, year, period, ientry, &nj, &nb, &ht, &cut, &trig, true, false, Sys_var);
			nb = min(nb,4);
			nj = min(nj,12);
			h2_NoHT->Fill(nb,nj,cut && trig);

			float progress = float(tot_ient)/(tot_ent);
			ProgressBar(progress);
			tot_ient++;
		}
	}
	cout << "\n Finished" <<endl;
	TCanvas *c = new TCanvas("c","c",1000,600);
	c->Divide(2,1);
	c->cd(1);
	h2_pureHT->SetTitle("With Pure HT Triggers");
	h2_pureHT->SetStats(0);
	h2_pureHT->SetMarkerSize(3);
	h2_pureHT->Draw("colz text");
	c->cd(2);
	h2_NoHT->SetTitle("Without Pure HT Triggers");
	h2_NoHT->SetStats(0);
	h2_NoHT->SetMarkerSize(3);
	h2_NoHT->Draw("samecolz text");

	c->Print("Plots/"+year+vnano+"_YieldComp.pdf");
	
	return 0;
}
