#include <iostream>
#include <vector>
#include <stdlib.h>

#include "TCanvas.h"
#include "TString.h"
#include "TStyle.h"
#include "TROOT.h"
#include "TH1.h"
#include "TH2.h"
#include "TGaxis.h"
#include "TLine.h"

#include "objects.hpp"
#include "periods.hpp"
#include "visual.hpp"

using namespace std;

int main(int argc, char *argv[]){
	if(strcmp(argv[1],"--help")==0){
		cout << "./run/trig_eff_1D.exe [pureHT on/off] [trigger efficiency correction turn on/off] [year] [nanoAOD version]" << endl;
	}
	else if(argc<5){
		cout << " [Error] not enough arguments! " << endl;
		cout << " --help may give you some help :) " << endl;
	}
	else{
		cout << " pure HT Trigger  : " << argv[1] << endl;
		cout << " year             : " << argv[3] << endl;
		cout << " nano AOD version : " << argv[4] << endl;
	
		TString PFHT  = argv[1];
		TString tec   = argv[2];
		TString year  = argv[3];
		TString vnano = argv[4];
		if(tec=="on"){
     cout << " trigger efficiency corrected " <<endl;
    }
		bool pureHT;
		if(PFHT=="on"){
			pureHT=true;
		}
		else if(PFHT=="off"){
			pureHT=false;
		}

		TH1D *h1_num = new TH1D("h1_num","h1_num",20,0,2000);
		TH1D *h1_den = new TH1D("h1_den","h1_den",20,0,2000);
		vector<TString> arr_per = periods(year);
		TFile *f = new TFile("hist_root/"+year+vnano+".root","read");
		int tot_ent(0), tot_ient(0);
		for(auto per : arr_per){
			TChain *ch = new TChain("Events");
			ch->Add("skimmed_files/"+year+vnano+"_"+per+"_Data.root");
			tot_ent += ch->GetEntries();
		}
		for(auto per : arr_per){
			TChain *ch = new TChain("Events");
			ch->Add("skimmed_files/"+year+vnano+"_"+per+"_Data.root");
			vector<void*> Sys_var = Syst_var(ch, year, per);

			for(int ientry = 0 ; ientry < ch->GetEntries() ; ientry++){
				int nb(0), nj(0);
				float ht(0), cor(0);
				bool cut(true),trig(true);
				GetSyst(ch, year, per, ientry, &nj, &nb, &ht, &cut, &trig, false, pureHT, Sys_var);
				tot_ient++;
				cor = TE_Corr(tec, f, nb, nj, ht, pureHT);
				h1_num->Fill(ht, cor*(trig && cut) );
				h1_den->Fill(ht, (cut) );
				ProgressBar(float(tot_ient)/tot_ent);
			}
		}
		cout<<"\033[1;32m completed   "<<endl;
		cout<<"\033[0m";
		f->Close();

		cout<<h1_num->Integral(8,8,"")<<"||"<<h1_num->Integral(9,14,"")<<"||"<<h1_num->Integral(15,20,"")<<endl;

		Double_t denmax = h1_den->GetMaximum();
		TH1D *h1_eff = dynamic_cast<TH1D*>(h1_num->Clone("h1_eff"));
		h1_eff->Divide(h1_num,h1_den,1,1,"B");
		TCanvas *c = new TCanvas("c","c",1000,1000);
		c->DrawFrame(0,0,2000,1.2);
		h1_eff->SetStats(0);
		h1_eff->SetMaximum(1.2);
		h1_eff->Draw("e");

		TGaxis *ax1 = new TGaxis(gPad->GetUxmax(),0,gPad->GetUxmax(),1.2,0,denmax*3,10510,"+L");//gPad->GetUxmax()
		ax1->SetLabelSize(0.04);
		ax1->SetLabelFont(40);                             // Setting Label's Font and Size.

		ax1->SetTitle("Events");
		ax1->SetTitleSize(0.04);
		ax1->SetTitleFont(40);
		ax1->SetTitleOffset(1.3);// Setting Title's Name, Font and Size.

		ax1->Draw("same");

		TLine *h1 = new TLine(gPad->GetUxmin(),1,gPad->GetUxmax(),1);
		h1->SetLineColor(kGray);
		h1->SetLineWidth(2);
		h1->Draw("same");
/*
		TLine *v1 = new TLine(700,gPad->GetUymin(),700,gPad->GetUymax());
		v1->SetLineColor(kGray);
		v1->SetLineWidth(2);
		v1->Draw("same");	

		TLine *v2 = new TLine(800,gPad->GetUymin(),800,gPad->GetUymax());
		v2->SetLineColor(kRed-9);
		v2->SetLineWidth(2);
		v2->Draw("same");
	
		TLine *v3 = new TLine(1400,gPad->GetUymin(),1400,gPad->GetUymax());
		v3->SetLineColor(kGray);
		v3->SetLineWidth(2);
		v3->Draw("same");	
*/
		h1_den->Scale(1/(3*denmax));
		h1_den->Draw("hist same");
		h1_num->SetFillStyle(3254);
		h1_num->SetFillColor(kGray);
		h1_num->Scale(1/(3*denmax));
		h1_num->Draw("hist same");
		c->Print("Plots/"+year+vnano+"_ht1D_cor_"+tec+".pdf");
	}
}
